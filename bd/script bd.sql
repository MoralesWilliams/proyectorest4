create table nota(
id_nota character varying(2),
nota character varying(200),
primary key(id_nota)
); 

create table Indicador(
id_indicador character varying(10),
indicador character varying(200),
id_nota character varying(2),
primary key(id_indicador),
foreign key(id_nota) references nota(id_nota) match full on update cascade on delete  restrict
);

create table entidad(
 id_entidad character varying(2),
 entidad character varying(40),
 primary key(id_entidad)
);

create table municipio(
id_entidad character varying(2),
id_municipio character varying(4),
municipio character varying(200),
primary key(id_municipio),
foreign key(id_entidad) references entidad(id_entidad) match full on update cascade on delete  restrict
);

create table nivel1(
 id_nivel1 character varying(2),
 tema_nivel1 character varying(100),
 primary key(id_nivel1)
);

create table nivel2(
 id_nivel1 character varying(2),
 id_nivel2 character varying(3),
 tema_nivel2 character varying(50),
 primary key(id_nivel2),
 foreign key(id_nivel1) references nivel1(id_nivel1) match full on update cascade on delete  restrict
);

create table nivel3(
 id_nivel2 character varying(3),
 id_nivel3 character varying(3),
 tema_nivel2 character varying(50),
 primary key(id_nivel3),
 foreign key(id_nivel2) references nivel2(id_nivel2) match full on update cascade on delete  restrict

);
 
 create table unidad_medida(
 id_unidad_medida character varying(3),
 unidad_medida character varying(50),
 primary key(id_unidad_medida)
 );

 create table valor_economico( 
 id_municipio character varying(4),
 id_identificador character varying(10),
 id_nivel3 character varying(3),
 id_unidad_medida character varying(3),
 cantidad  numeric(20,2),
 foreign key(id_municipio) references municipio(id_municipio) match full on update cascade on delete  restrict,
 foreign key(id_indicador) references indicador(id_indicador) match full on update cascade on delete  restrict,
 foreign key(id_nivel3) references nivel3(id_nivel3) match full on update cascade on delete  restrict,
 foreign key(id_unidad_medida) references unidad_medida(id_unidad_medida) match full on update cascade on delete  restrict
);
 
 
 