<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$app->get('/notas', function() use ($app) {


    $headers = $app->request()->headers('Accept');
    $db = new Conection();
    $contenido = $db->getNotas();

    if ($headers == 'application/json') {

        $app->response()['Content-Type'] = 'application/json';
        $app->response()->status(200);
        $resultado = array(
            'notas' => $contenido
        );
        $app->response()->body(json_encode($resultado));
    }
    //if($app_response='application/xml')
    else {
        $app->response()->status(200);
        $app->response()['Content-Type'] = 'application/xml';
        //var_dump($contenido);
        xml_datos($app, "notas", "id_nota", $contenido);
    }
});



$app->get('/indicadores/:id/notas', function($id) use ($app) {
    if (!is_numeric($id)) {
        validar_id($app,"notas");
        $app->response()->status(400);
    }
    else {
        $headers = $app->request()->headers('Accept');
        $db = new Conection();
        $contenido = $db->getNotasDeUnIndicadorEspecifico($id);

        if ($headers == 'application/json') {

            $app->response()['Content-Type'] = 'application/json';
            $app->response()->status(200);
            $resultado = array(
                'nota' => $contenido
            );
            $app->response()->body(json_encode($resultado));
        }
        //if($app_response='application/xml')
        else {
            $app->response()->status(200);
            $app->response()['Content-Type'] = 'application/xml';
            //var_dump($contenido);
            xml_datos($app, "nota", "id_nota", $contenido);
        }
    }
});