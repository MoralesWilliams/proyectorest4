<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$app->get('/temas', function() use ($app) {


    $headers = $app->request()->headers('Accept');
    $db = new Conection();
    $contenido = $db->getNivel1();

    if ($headers == 'application/json') {

        $app->response()['Content-Type'] = 'application/json';
        $app->response()->status(200);
        $resultado = array(
            'temas' => $contenido
        );
        $app->response()->body(json_encode($resultado));
    }
    //if($app_response='application/xml')
    else {
        $app->response()->status(200);
        $app->response()['Content-Type'] = 'application/xml';
        //var_dump($contenido);
        xml_datos($app, "temas", "id_nivel1", $contenido);
    }
});


$app->get('/temas/:id', function($id) use ($app) {
    if (!is_numeric($id)) {
        validar_id($app,"temas");
        $app->response()->status(400);
    }
    else {
        $headers = $app->request()->headers('Accept');
        $db = new Conection();
        $contenido = $db->getNivel1Especifico($id);

        if ($headers == 'application/json') {

            $app->response()['Content-Type'] = 'application/json';
            $app->response()->status(200);
            $resultado = array(
                'tema' => $contenido
            );
            $app->response()->body(json_encode($resultado));
        }
        //if($app_response='application/xml')
        else {
            $app->response()->status(200);
            $app->response()['Content-Type'] = 'application/xml';
            //var_dump($contenido);
            xml_datos($app, "tema", "id_tema", $contenido);
        }
    }
});



