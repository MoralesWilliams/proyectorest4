<?php

/*
 * Recurso de entidades
 */


$app->get('/api/', function() use ($app) {
    
        $app->response()->redirect('http://192.168.1.40/rest/censoseconomicos/api/vistas/index.php');
                
       
});

$app->get('/entidades', function() use ($app) {
    $headers = $app->request()->headers('Accept');
    $db = new Conection();
    $contenido = $db->getEntidades($app);

    if ($headers == 'application/json') {

        $app->response()['Content-Type'] = 'application/json';
        $app->response()->status(200);
        $resultado = array(
            'entidades' => $contenido
        );
        $app->response()->body(json_encode($resultado));
    }
    //if($app_response='application/xml')
    else {
        $app->response()->status(200);
        $app->response()['Content-Type'] = 'application/xml';
        //var_dump($contenido);
        xml_datos($app, "entidades", "id_entidad", $contenido);
    }
});


$app->get('/entidades/:id', function($id) use ($app) {
    if (!is_numeric($id))
        validar_id($app, "entiodades");
    else {
        $headers = $app->request()->headers('Accept');
        $db = new Conection();
        $contenido = $db->getEntidadEspecifica($id);

        if ($headers == 'application/json') {

            $app->response()['Content-Type'] = 'application/json';
            $app->response()->status(200);
            $resultado = array(
                'entidades' => $contenido
            );
            $app->response()->body(json_encode($resultado));
        }
        //if($app_response='application/xml')
        else {
            $app->response()->status(200);
            $app->response()['Content-Type'] = 'application/xml';
            //var_dump($contenido);
            xml_datos($app, "entidades", "id_entidad", $contenido);
        }
    }
});

/*
 * 
  {
  "entidad":
     { 
       "nombre":"emergente"
     }
  }
 * 
 * 
 * 
  <entidad>
    <nombre>jejeje</nombre>
  </entidad>
 */

$app->post('/entidades', function() use ($app) {
    $headers = $app->request()->headers('Accept');
    $db = new Conection();
    $nueva_entidad = $app->request()->getBody();
    if ($app->request()->headers('Content-Type') == 'application/json') {
        if ($headers == 'application/json') {
            $datos = json_decode($nueva_entidad);

            $nombre = $datos->entidad->nombre;


            $consulta = $db->val_tabla("entidad", "nombre", $nombre);
            //var_dump($consulta);        

            if (count($consulta) >= 1) {
                $resultado = array(
                    'mensaje' => "fracaso"
                );
                $app->response()->status(400);
                $app->response()->body(json_encode($resultado));
            } else {

                $db->setPostEntidad($nombre);
                $db = new Conection();
                $ultimo = $db->obtenerUltimoRegistro("entidad", "id_entidad");
                $ultimo = $ultimo[0]["id_entidad"];
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/' . $ultimo, 303);
                $app->response()->status(303);
            }
        } else {
            $datos = json_decode($nueva_entidad);
            $nombre = $datos->entidad->nombre;


            $consulta = $db->val_tabla("entidad", "nombre", $nombre);
            if (count($consulta) >= 1) {
                $res = array(
                    "msj" => "fracaso");
                $contenido = [$res];
                $app->response()->status(400);
                xml_datos($app, "entidad", "", $contenido);
            } else {
                $db->setPostEntidad($nombre);
                $db = new Conection();
                $ultimo = $db->obtenerUltimoRegistro("entidad", "id_entidad");
                $ultimo = $ultimo[0]["id_entidad"];
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/' . $ultimo, 303);
                $app->response()->status(303);
            }
        }
    } else {
        $nueva_entidad = $app->request()->getBody();
        if ($headers == 'application/json') {
            $datos = new DOMDocument('1.0', 'UTF - 8');
            $datos->loadXML($nueva_entidad);

            //var_dump($datos) ;
            $nodo_nombre = $datos->getElementsByTagName('nombre');
            $nombre_item = $nodo_nombre->item(0);
            $nombre = $nombre_item->nodeValue;

            $consulta = $db->val_entidad("nombre", $nombre);
            //var_dump($consulta);
            if (count($consulta) >= 1) {
                $resultado = array(
                    'mensaje' => "fracaso",
                );
                $app->response()->status(400);
                $app->response()->body(json_encode($resultado));
            } else {

                $db->setPostEntidad($nombre);
                $db = new Conection();
                $ultimo = $db->obtenerUltimoRegistro("entidad", "id_entidad");
                $ultimo = $ultimo[0]["id_entidad"];
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/' . $ultimo, 303);
                $app->response()->status(303);
            }
        } else {
            $datos = new DOMDocument('1.0', 'UTF - 8');
            $datos->loadXML($nueva_entidad);
            $nodo_nombre = $datos->getElementsByTagName('nombre');
            $nombre_item = $nodo_nombre->item(0);
            $nombre = $nombre_item->nodeValue;


            $consulta = $db->val_entidad("nombre", $nombre);
            if (count($consulta) >= 1) {
                $res = array(
                    "msj" => "fracaso");
                $contenido = [$res];
                $app->response()->status(400);
                xml_datos($app, "entidad", "", $contenido);
            } else {
                $db->setPostEntidad($nombre);
                $db = new Conection();
                $ultimo = $db->obtenerUltimoRegistro("entidad", "id_entidad");
                $ultimo = $ultimo[0]["id_entidad"];
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/' . $ultimo, 303);
                $app->response()->status(303);
            }
        }
    }
});



$app->delete('/entidades/:id', function($id) use($app) {
    $db = new Conection();
    $headers = $app->request()->headers('Accept');
    if (!is_numeric($id))
        validar_id($app, "entiodades");
    else {
        $consulta = $db->val_tabla("entidad", "id_entidad", $id);
        if ($headers == 'application/json') {
            $app->response()['Content-Type'] = 'application/json';
            if (count($consulta) >= 1) {
                $db->deleteEntidad($id);

                $app->response()->status(200);

                $resultado = array(
                    'mensaje' => "exito"
                );
                $contenido = array(
                    "entidades" => $resultado
                );
                $app->response()->body(json_encode($contenido));
            } else {
                $resultado = array(
                    'mensaje' => "fracaso"
                );
                $app->response()->status(400);
                $app->response()->body(json_encode($resultado));
            }
        } else {
            $app->response()['Content-Type'] = 'application/xml';
            if (count($consulta) >= 1) {
                $db->deleteEntidad($id);
                $resultado = array(
                    'mensaje' => "exito"
                );
                $contenido = [$resultado];
                $app->response()->status(200);
                xml_datos($app, "entidad", "", $contenido);
            } else {
                $resultado = array(
                    'mensaje' => "fracaso"
                );
                $contenido = [$resultado];
                $app->response()->status(400);
                xml_datos($app, "entidad", "", $contenido);
            }
        }
    }
});

/*
  $app->get('/ultimo', function ()use($app) {
  $db = new Conection();
  $data = $db->obtenerUltimoRegistro("entidad", "id_entidad");
  echo$data[0]["id_entidad"];
  });
 */

/*
 * ===========PUT
 */

/*
 * <entidad>
  <nuevo_nombre>yucatan</nuevo_nombre>
  </entidad>  
 */


$app->put('/entidades/:id', function($id) use ($app) {
    $headers = $app->request()->headers('Accept');
    $db = new Conection();
    $entidad_actual = $app->request()->getBody();
    if ($app->request()->headers('Content-Type') == 'application/json') {
        if ($headers == 'application/json') {
            $datos = json_decode($entidad_actual);

            $nombre = $datos->entidad->nuevo_nombre;
            echo $nombre;

            $consulta = $db->val_tabla("entidad", "nombre", $nombre);
            //var_dump($consulta);        

            if (count($consulta) >= 1) {
                $resultado = array(
                    'mensaje' => "fracaso"
                );
                $app->response()->status(400);
                $app->response()->body(json_encode($resultado));
            } else {
                 $db = new Conection();
                $db->setPutEntidad($id,$nombre);
               
                
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/'.$id, 303);
                $app->response()->status(303);
            }
        } else {
            $datos = json_decode($entidad_actual);
            $nombre = $datos->entidad->nuevo_nombre;


            $consulta = $db->val_tabla("entidad", "nombre", $nombre);
            if (count($consulta) >= 1) {
                $res = array(
                    "msj" => "fracaso");
                $contenido = [$res];
                $app->response()->status(400);
                xml_datos($app, "entidad", "", $contenido);
            } else {
                $db = new Conection();
                $db->setPutEntidad($id,$nombre);
               
                
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/'.$id, 303);
                $app->response()->status(303);
            }
        }
    } else {
        $entidad_actual = $app->request()->getBody();
        if ($headers == 'application/json') {
            $datos = new DOMDocument('1.0', 'UTF - 8');
            $datos->loadXML($entidad_actual);
            
            //var_dump($datos) ;
            $nodo_nombre = $datos->getElementsByTagName('nuevo_nombre');
            $nombre_item = $nodo_nombre->item(0);
            $nombre = $nombre_item->nodeValue;

            $consulta = $db->val_entidad("nombre", $nombre);
            //var_dump($consulta);
            if (count($consulta) >= 1) {
                $resultado = array(
                    'mensaje' => "fracaso",
                );
                $app->response()->status(400);
                $app->response()->body(json_encode($resultado));
            } else {

               $db = new Conection();
                $db->setPutEntidad($id,$nombre);
               
                
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/'.$id, 303);
                $app->response()->status(303);
            }
        } else {
            $datos = new DOMDocument('1.0', 'UTF - 8');
            $datos->loadXML($entidad_actual);
            $nodo_nombre = $datos->getElementsByTagName('nuevo_nombre');
            $nombre_item = $nodo_nombre->item(0);
            $nombre = $nombre_item->nodeValue;


            $consulta = $db->val_entidad("nombre", $nombre);
            if (count($consulta) >= 1) {
                $res = array(
                    "msj" => "fracaso");
                $contenido = [$res];
                $app->response()->status(400);
                xml_datos($app, "entidad", "", $contenido);
            } else {
                $db = new Conection();
                $db->setPutEntidad($id,$nombre);
               
                
                $app->response()->redirect('http://localhost/rest/censoseconomicos/entidades/'.$id, 303);
                $app->response()->status(303);
            }
        }
    }
});





